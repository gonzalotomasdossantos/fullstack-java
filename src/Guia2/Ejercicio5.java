/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia2;

import static Guia2.Ejercicio5.fibonacci;
import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio5 {
    public static void main(String[] args) throws Exception {
     /* Ejercicio 5: Escribir un algoritmo que reciba un valor n y devuelva los primeros n terminos
        de la sucesion de fibonacci */
     
     int numero;
     Scanner s = new Scanner(System.in);
     
     System.out.println("Introduce el número de términos que quieres ver");
     numero = s.nextInt();
     
     if (numero < 0) {
        throw new Exception("El número ingresado debe ser positivo");
     }
    
     for (int i = 1; i <= numero; i++) {
         System.out.println(fibonacci(i));
     }
     
    }
    
    public static int fibonacci(int termino) {
        
        int fibonacci;
        
        if (termino == 0 || termino == 1) {
            fibonacci = termino;
        } else {
            fibonacci = fibonacci(termino-1) + fibonacci(termino-2);
        }

        return fibonacci;
    }

}
