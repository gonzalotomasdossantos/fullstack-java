/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia2;

/**
 *
 * @author HDCFc
 */
public class Ejercicio6 {
    public static void main(String[] args) {
     /* Ejercicio 6: Hacer un algoritmo que imprima los números primos menores que 200 */ 
     
        System.out.println("Listado de los 200 primeros numeros primos: ");
     
        for (int i = 1; i <= 200; i++) {
            if (esPrimo(i)) {
                System.out.println(i);
            }
        }
     
           
    }
    
    public static boolean esPrimo(int numero) {
        int numeroDivisores = 0;
        boolean primo;
        
        for (int i = 1; i <= numero; i++) {
          if (numero % i == 0) {
              numeroDivisores++;
          }
        }
        
        primo = numeroDivisores <= 2;
        
        return primo;
      
    }
}

