/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia2;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio2 {
    public static void main(String[] args){
     /* Ejercicio 2: Escribir un programa que lea una palabra por teclado y determine si
es un palíndromo. */
     
     
     Scanner s = new Scanner(System.in);
     String palabraEnReversa = "";
     System.out.println("Introduce la palabra");
     String palabra = s.nextLine();
     
     int limite = palabra.length() - 1;
     
     for (int i = limite; i >= 0; i--) {
         palabraEnReversa = palabraEnReversa + palabra.charAt(i);
     }
     
     if (palabra.equals(palabraEnReversa)) {
         System.out.println("La palabra es un palindromo");
     } else {
         System.out.println("La palabra no es un palindromo");
     }
    
    }
}
