/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia2;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio7 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String matrizCodigos[][] = new String[8][2];
        /* Llenando la fila de los nombres de los animales */
        matrizCodigos[0][0] = "Piton";
        matrizCodigos[1][0] = "Gato";
        matrizCodigos[2][0] = "Caballo";
        matrizCodigos[3][0] = "Alce";
        matrizCodigos[4][0] = "Tortuga";
        matrizCodigos[5][0] = "Caracol";
        matrizCodigos[6][0] = "Cóndor";
        matrizCodigos[7][0] = "Leon";
        /* Llenando la fila de los códigos de los animales */
        matrizCodigos[0][1] = "NONOSI"; /* No Herviboro, no Mamifero, Si Doméstico */ 
        matrizCodigos[1][1] = "NOSISI"; /* No Herviboro, Si Mamifero, Si Doméstico */
        matrizCodigos[2][1] = "SISISI"; /* etc... */
        matrizCodigos[3][1] = "SISINO";
        matrizCodigos[4][1] = "SINOSI";
        matrizCodigos[5][1] = "SINONO";
        matrizCodigos[6][1] = "NONONO";
        matrizCodigos[7][1] = "NOSINO";
        
        String respuestaUsuario = "";
        
        System.out.println("¿El animal es Herviboro? SI / NO");
        
        respuestaUsuario = respuestaUsuario + s.nextLine().toUpperCase();
        
        System.out.println("¿El animal es mamífero? SI / NO");
        
        respuestaUsuario = respuestaUsuario + s.nextLine().toUpperCase();

        System.out.println("¿El animal es doméstico? SI / NO");
        
        respuestaUsuario = respuestaUsuario + s.nextLine().toUpperCase();     

    
        String animalEncontrado = ""; 
        
        for (int i = 0; i <= 7; i++) {
            if(matrizCodigos[i][1].equals(respuestaUsuario)){
              animalEncontrado = matrizCodigos[i][0];
            }            
        }
        
        if (!"".equals(animalEncontrado)) {
            System.out.println("Has descripto a: " + animalEncontrado);
        } else {
            System.out.println("No has descripto a ningún animal");
        }
         
        
    }
    
}
