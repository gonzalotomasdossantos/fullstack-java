/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia2;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio1 {
    public static void main(String[] args) {
     /* Ejercicio 1: Escribir un programa que reciba un número entero por teclado. A
continuación, mostrar la tabla de multiplicar de ese número. */
     
     Scanner s = new Scanner(System.in);
     int number;
     number = s.nextInt();
     
     for (int i = 1; i <= 10; i++) {
         System.out.println(number + "*" + i + "= " + (number * i));
     }
     
    }
}
