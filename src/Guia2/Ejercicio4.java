/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia2;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio4 {
     public static void main(String[] args) throws Exception {
     /* Ejercicio 4: Escribir un algoritmo que calcule el factorial de un número ingresado
por teclado. */ 
     
     int numero, factorial;
     Scanner s = new Scanner(System.in);
     
     System.out.println("Ingrese el número que desea calcular el factorial: ");
     
     numero = s.nextInt();
     factorial = 1;
     
     if (numero > 0) {
       for (int i = 1; i <= numero; i++) {
            factorial = factorial * i;    
        }
     } else if (numero == 0) {
       factorial = 1;
     } else {
       throw new Exception("El número debe ser positivo para calcular su factorial.");
     }
     
     
    System.out.println("El factorial es: " + factorial);
 
     }
}
