/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia2;

import java.util.Arrays;


/**
 *
 * @author HDCFc
 */

public class Ejercicio3 {
    public static void main(String[] args) {
    /* Ejercicio 3: Escribir un programa que ordene un arreglo de números de manera ascendente */       
    
    int[] arregloDeNumeros = {1232, 4121, 231, 32, 231, 2};
    int n = arregloDeNumeros.length;
    
    for (int i = 0; i <= (n - 1); i++) {
       for (int j = 0; j < (n-1-i); j++) {
         if (arregloDeNumeros[j] > arregloDeNumeros[j+1]) {
            int aux = arregloDeNumeros[j];
            arregloDeNumeros[j] = arregloDeNumeros[j+1];
            arregloDeNumeros[j+1] = aux;
         }
       }
    }    
    
    System.out.println("Arreglo ordenado: " + Arrays.toString(arregloDeNumeros));
    
    }
   
}