-- Apuntes Clase 5 curso de Java -- 

-- Vectores y matrices --

-- Arreglos:

Son colecciones de datos que nos permiten agrupar variables relacionadas entre sí
que comparten el mismo tipo. 


int edadAlumnos[] = [18, 18, 28]

-- Vectores:

Son arreglos unidimensionales. Cada valor individual se accede mediante la posición que ocupa;

int[] vectorDeNumeros = new int[numeroDeElementos];

int otroVector[] = {10, 20, 30, 40, 50};

Para guardar un valor en una posición usamos [] indicando a qué posición nos referimos

otroVector[0] // 10

System.out.println(otroVector[1]) // 20

// Matrices:

Son arreglos bidimensionales.

int[][] unaMatriz = new int[2][2] 

Dos fila y dos columnas 

2  2
2  0

// Las posiciones de un arreglo van de 0 a (n - 1 ), donde n es el número de elementos del arreglo.


// Manejo de arreglos:

- copyOf: copia un arreglo y lo devuelve en otro nuevo
- equals: compara dos arreglos ya que no podemos usar ==
- sort: devuelve los valores ordenados de un arreglo

--- CASTING --- 

- Procedimiento para transformar una variable primitiva a otra. También se usa para transformar un objeto
de una clase a otro, siempre haya una relación de herencia entre ellas;

- casting implícito: Se coloca un valor pequeño en un contenedor grande

int num1 = 100;
long num2 = num1;

- casting explícito: Es necesario que se escriba código para que la conversión se realice

int num1 = 100;
short num2 = (short) num1;

- parsing: es el procedimiento para transformar una cadena de texto en un tipo primitivo. Si no se
puede, provoca una NumberFormatException.


-- FUNCIONES: Bloques para organizar el código. Realizan una sola tarea en específico. 

Partes:

- nombre
- paramteros (variables que recibe como entrada)
- tipo de retorno (tipo de valor que devuelve)
- contiene un bloque de código encerrado entre llaves que definen el codigo a ejecutar

public static int Suma(int num1, int num2) {
  int resultado = num1 + num2;
  return resultado;
}

System.out.print(suma(10, 10)) // 20 




 
