/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia1;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio4 {
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        /* Ejercicio 4: Promedio tres estaturas */
        Scanner s = new Scanner(System.in);
        Double estatura1, estatura2, estatura3, promedio ;
                
        System.out.print("Introduce la estatura 1 en cm: ");
        estatura1 = Double.valueOf(s.nextLine());
        System.out.print("Introduce la estatura 2 en cm: ");
        estatura2 = Double.valueOf(s.nextLine());
        System.out.print("Introduce la estatura 3 en cm: ");
        estatura3 = Double.valueOf(s.nextLine());
      
        promedio = (estatura1 + estatura2 + estatura3) / 3;
        
        System.out.println("El promedio de altura es de " + promedio + "cm.");
    };
}
