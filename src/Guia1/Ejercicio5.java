/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia1;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio5 {
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        /* Ejercicio 5: Mostrar area y perimetro a partir de radio */
        Double radio, perimetro, area;
        System.out.print("Introduce el radio: ");
        radio = Double.valueOf(s.nextLine());
        perimetro = 2 * Math.PI * (radio * radio);
        area = Math.PI * Math.pow(radio, 2);

        /* ------------------------------------
        El cálculo del perimetro no es correcto
        ------------------------------------ */
        System.out.println("El área es " + Math.floor(area));
        System.out.println("El perímetro es " + Math.floor(perimetro));
    };
}
