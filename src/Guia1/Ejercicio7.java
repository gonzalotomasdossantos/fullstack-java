package Guia1;

import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author HDCFc
 */
public class Ejercicio7 {
    public static void main(String[] args) {
        /* Ejercicio 7: Escribir un programa que reciba el valor de dos edades y las guarde
            en dos variables. Luego el programa debe intercambiar los valores de
            ambas variables y mostrarlas por pantalla. Por ejemplo, si el usuario
            ingresó los valores edad1 = 24 y edad2 = 35, el programa deberá
            mostrar edad1 = 35 y edad2 = 24. */ 
            
        int edad1, edad2, edadAux;
        
        Scanner s = new Scanner(System.in);
        
        
        System.out.println("Introduce la edad 1");
        edad1 = s.nextInt();
        System.out.println("Introduce la edad 2");
        edad2 = s.nextInt();
        
        edadAux = edad1;
        
        edad1 = edad2;
        edad2 = edadAux;
        
        System.out.println("Hemos intercambiado tus variables. Ahora edad 1 es " + edad1 + " y edad 2 es " + edad2);
        
    }
}
