/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia1;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio9 {
    public static void main(String[] args) {
        /* Ejercicio 9: A partir de una cantidad de pesos que el usuario ingresa a través del
teclado se debe obtener su equivalente en dólares, en euros, en
guaraníes y en reales. */
        Float pesos, tasa_dolares, tasa_euros, tasa_guaranies, tasa_reales,dolares, euros, guaranies, reales;
        Scanner s = new Scanner(System.in);
        
        tasa_dolares = 231.68f;
        tasa_euros = 250.69f;
        tasa_guaranies = 31f;
        tasa_reales = 46.81f;
        
        System.out.println("Introduce la cantidad de pesos que deseas convertir");
        pesos = s.nextFloat();

        /* -------------------------------
        La conversión a guaraníes está mal
        ------------------------------- */
        dolares = pesos / tasa_dolares;
        euros = pesos / tasa_euros;
        reales = pesos / tasa_reales;
        guaranies = pesos / tasa_guaranies;

        System.out.println(pesos + "$ son equivalentes a :");
        System.out.printf("%.2f", dolares);
        System.out.println(" dolares.");
        System.out.printf("%.2f", euros);
        System.out.println(" euros.");
        System.out.printf("%.2f", reales);
        System.out.println(" reales.");
        System.out.printf("%.2f", guaranies);
        System.out.println(" guaraníes.");
    }
}
