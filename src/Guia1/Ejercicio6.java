/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia1;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio6 {
    public static void main(String[] args){
      /* Ejercicio 6: Pedir al usuario que ingrese el precio de un producto y el porcentaje
        de descuento. A continuación mostrar por pantalla el importe
        descontado y el importe a pagar. */
      
      Double precioProducto, porcentajeDeDescuento, precioFinal, cantidadDescontada;
      
      Scanner s = new Scanner(System.in);
      
      System.out.println("Introduce el precio del producto: ");
      precioProducto = Double.valueOf(s.nextLine());
      System.out.println("Ahora introduce el porcentaje de descuento: ");
      porcentajeDeDescuento = Double.valueOf(s.nextLine());
      cantidadDescontada = (precioProducto * porcentajeDeDescuento) / 100;
      precioFinal = precioProducto - cantidadDescontada;
    

      System.out.println("La cantidad de descuento que se aplicará es $" + cantidadDescontada);
      System.out.println("Por lo tanto, el precio final será de $" + precioFinal);
      
    }
}
