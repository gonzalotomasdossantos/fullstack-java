/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia1;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio2 {
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        /* Ejercicio 2: Saludar */
        
        Scanner s = new Scanner(System.in);
        System.out.print("Introduce tu nombre: ");
        String nombre = s.nextLine();
        System.out.println("Hola, " + nombre);
    };
}
