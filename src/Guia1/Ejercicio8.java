/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia1;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio8 {
    public static void main(String[] args){
     /* Ejercicio 8: Pedir al usuario que ingrese una temperatura en grados Celsius y
mostrar por pantalla su equivalente en kelvin y grados Fahrenheit. Las
fórmulas para conversiones son: */ 
     
     Scanner s = new Scanner(System.in);
     Double celsius, kelvin, fahrenheit;
     
     System.out.println("Introduce la temperatura en grados celsius");
     celsius = s.nextDouble();
     
     kelvin =  273.15 + celsius;
     fahrenheit = 1.8 * celsius;
     
     System.out.println("Celsius: " + celsius);
     System.out.println("Fahrenheit: " + fahrenheit);
     System.out.println("Kelvin: " + kelvin);

    }
}
