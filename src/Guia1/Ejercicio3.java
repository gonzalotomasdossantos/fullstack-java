/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guia1;

import java.util.Scanner;

/**
 *
 * @author HDCFc
 */
public class Ejercicio3 {
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        /* Ejercicio 3: Leer dos numeros y hacer calculos */
        Scanner s = new Scanner(System.in);
        
        Double numero1, numero2, suma, resta, multiplicacion, division;
        
        System.out.print("Ingrese el número 1: ");
        numero1 = Double.valueOf(s.nextLine());
        System.out.print("Ingrese el número 2: ");
        numero2 = Double.valueOf(s.nextLine());
        
        suma = numero1 + numero2;
        resta = numero1 - numero2;
        multiplicacion = numero1 * numero2;
        division = numero1 / numero2;
        
        System.out.println(numero1 + " + " + numero2 + "  = " + suma);
        System.out.println(numero1 + " - " + numero2 + "  = " + resta);
        System.out.println(numero1 + " * " + numero2 + "  = " + multiplicacion);
        System.out.println(numero1 + " / " + numero2 + "  = " + division);
    };
}
